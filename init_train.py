import os
import sys
import configparser

config = configparser.ConfigParser()
config.readfp(open(r'./configs.txt'))
experiment_name = config.get('experiment', 'name')
nohup = config.getboolean('training_settings', 'nohup')

result_dir = experiment_name

run_GPU = '' if sys.platform == 'win32' else 'THEANO_FLAGS=device=cuda,floatX=float32'

if os.path.exists(result_dir):
    print ("Experiment directory already exists")
elif sys.platform == 'win32':
    os.system('mkdir ' + result_dir)
else:
    print("Creating experiment directory...\n")
    os.system('mkdir -p ' + result_dir)

print ("Copying the configuration file to the experiment folder")

if sys.platform == 'win32':
    os.system('copy configs.txt .\\' + experiment_name+'\\'
              + experiment_name + '_configs.txt')
else:
    os.system('cp configs.txt ./' + experiment_name + '/'
              + experiment_name + '_configs.txt')
if nohup:
    print ("\n2. Run the training on GPU with nohup")
    print ("\n3. PLEASE DO NOT CLOSE THIS TERMINAL WINDOW"
           + "\n TRAINING IS IN PROGRESS")
    os.system(run_GPU + ' nohup python -u ./train_UNET.py > ' +
              './'+experiment_name+'/'+experiment_name+'_training.nohup')
else:
    print ("\n2. Run the training on GPU (no nohup)")
    os.system(run_GPU + ' python ./train_UNET.py')
