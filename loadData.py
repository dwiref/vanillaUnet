import os
import h5py
import cv2
import numpy as np

from multiprocessing import Pool
from PIL import Image
import matplotlib.pyplot as plt

imgPath = "./IDRiD/train/images/"
groundPath = "./IDRiD/train/ground"
outPath = "./data/"
n_imgs = 63
height = 1280
width = 1920


def load_data(dir, type):
    imgs = np.empty((n_imgs, height, width))
    for path, subdirs, files in os.walk(dir):
        files = sorted(files)
        for i in range(len(files)):
            im_name = files[i]
            print(type + " : " + im_name)
            img = cv2.imread(dir + im_name)[:, :, 1]
            img = cv2.resize(img, (width, height), interpolation=cv2.INTER_AREA)
            imgs[i] = np.asarray(img)
    outName = outPath + "groundTruth.hdf5"
    write_hdf5(imgs, outName)
    return imgs


def write_hdf5(img, filename):
    with h5py.File(filename, "w") as f:
        f.create_dataset("image", data=img, dtype=img.dtype)


print("Beginning preprocessing...")

for path, subdirs, files in os.walk(imgPath):
    files = sorted(files)

print(range(len(files)))

data = np.empty((height, width, 3))


def doData(i):
    print("original image: " + files[i])
    img = cv2.imread(imgPath + files[i])[:, :, 1]
    img = cv2.resize(img, (width, height), interpolation=cv2.INTER_AREA)
    print(img.shape)
    im, e, c, o = vesselRiesz(img)
    img = imNormalize(img, 0.0, 1.0)
    data[:, :, 0] = img
    data[:, :, 1] = e
    data[:, :, 2] = c

    return data


with Pool(processes=22) as pool:
    data = pool.map(doData, range(len(files)))
    pool.close()
    pool.terminate()
    pool.join()
    print(len(data))

data = np.reshape(np.asarray(data), (n_imgs, height, width, 3))
write_hdf5(data, (outPath + "features.hdf5"))

print("Complete!")
