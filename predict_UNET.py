import numpy as np
import configparser
from matplotlib import pyplot as plt
import h5py
import random

from keras.models import model_from_json
from keras.models import Model
from sklearn import preprocessing
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import jaccard_similarity_score
from sklearn.metrics import f1_score
import cv2
import sys

# Get configuration variables
config = configparser.RawConfigParser()
config.read('configs.txt')
images_dir = config.get('data_paths', 'images_dir')
ground_dir = config.get('data_paths', 'ground_dir')
experiment_name = config.get('experiment', 'name')

# Fetch the model
model = model_from_json(open('./' + experiment_name + '/' + experiment_name
                             + '_architecture.json').read())
model.load_weights('./' + experiment_name + '/' + experiment_name + '_'
                   + 'best_wt.h5')


def load_hdf5(infile):
    with h5py.File(infile, "r") as f:
        # "with" close the file after its nested commands
        return f["image"][()]


# Fetch data from hdf5 files
X_train = load_hdf5(images_dir)
Y_train = load_hdf5(ground_dir)

# Start prediction
preds_train = model.predict(X_train[:int(X_train.shape[0]*0.9)], verbose=1)
preds_val = model.predict(X_train[int(X_train.shape[0]*0.9):], verbose=1)
# preds_test = model.predict(X_test, verbose=1)

# Threshold the predictions
preds_train_t = (preds_train > 0.5).astype(np.uint8)
preds_val_t = (preds_val > 0.5).astype(np.uint8)
# preds_test_t = (preds_test > 0.5).astype(np.uint8)

plt.gcf().clear()
ix = random.randint(0, len(preds_train_t))
plt.imshow(X_train[ix])
plt.figure(), plt.imshow(np.squeeze(Y_train[ix]))
plt.figure(), plt.imshow(np.squeeze(preds_train_t[ix]))
plt.show()
