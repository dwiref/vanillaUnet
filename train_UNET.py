import os
import configparser
import h5py
import numpy as np
import tensorflow as tf

from pureUnet import get_unet

from keras.callbacks import ModelCheckpoint, LearningRateScheduler
from keras import backend as K
from keras.utils.vis_utils import plot_model as plot
from keras.optimizers import SGD
from keras.models import load_model
from keras.losses import binary_crossentropy
import os
import configparser
import h5py

import tensorflow as tf

from pureUnet import get_unet

from keras.callbacks import ModelCheckpoint, LearningRateScheduler
from keras import backend as K
from keras.utils.vis_utils import plot_model as plot
from keras.optimizers import SGD
from keras.models import load_model
from keras.losses import binary_crossentropy

# Set which GPU to use
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "1"

# Get configuration variables
config = configparser.RawConfigParser()
config.read('config.txt')
images_dir = config.get('data paths', 'images_dir')
ground_dir = config.get('data_paths', 'ground_dir')
experiment_name = config.get('experiment', 'name')
N_epochs = int(config.get('training_settings', 'N_epochs'))
batch_size = 1
height = int(config.get('training_settings', 'height'))
width = int(config.get('training_settings', 'width'))
n_ch = int(config.get('training_settings', 'n_ch'))


def load_hdf5(infile):
    with h5py.File(infile, "r") as f:
        # "with" close the file after its nested commands
        return f["image"][()]


# Fetch data from hdf5 files
X_train = load_hdf5(images_dir)
Y_train = load_hdf5(ground_dir)

# Load model
model = get_unet(height, width, n_ch)
print("Checkpoint: shape of final output from the model: ")
print(model.output_shape)

# Save the model architecture to file
plot(model, to_file='./'+experiment_name+'/'+experiment_name + '_model.png')
# ^check how the model looks like
json_string = model.to_json()
open('./'+experiment_name+'/'+experiment_name + '_architecture.json',
     'w').write(json_string)

# Scheduler


def step_decay(epoch):
    learning_rate = 0.01
    if epoch == 100:
        return 0.005
    else:
        return learning_rate


rate_drop = LearningRateScheduler(step_decay)

# Begin training
checkpointer = ModelCheckpoint(filepath='./experiment_name'+'/'+'_best_wt.h5',
                               verbose=1, monitor='val_loss', mode='auto',
                               save_best_only=True)
model.fit(X_train, Y_train, epochs=N_epochs, batch_size=batch_size, verbose=2,
          shuffle=True, validation_split=0.1, callbacks=[checkpointer])

# Save the model and weights for testing
model.save_weights('./'+experiment_name+'/'+'_final_wt.h5', overwrite=True)

# Set which GPU to use
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "1"

# Get configuration variables
config = configparser.RawConfigParser()
config.read('configs.txt')
images_dir = config.get('data_paths', 'images_dir')
ground_dir = config.get('data_paths', 'ground_dir')
experiment_name = config.get('experiment', 'name')
N_epochs = int(config.get('training_settings', 'N_epochs'))
batch_size = 2
height = int(config.get('training_settings', 'height'))
width = int(config.get('training_settings', 'width'))
n_ch = int(config.get('training_settings', 'n_ch'))


def load_hdf5(infile):
    with h5py.File(infile, "r") as f:
        # "with" close the file after its nested commands
        return f["image"][()]


# Fetch data from hdf5 files
X_train = load_hdf5(images_dir)
ytrain = load_hdf5(ground_dir)

Y_train = np.transpose(ytrain, (0, 2, 3, 1))

print("CHECK THIS")
print(X_train.shape)
print(Y_train.shape)


# Load model
model = get_unet(height, width, n_ch)
print("Checkpoint: shape of final output from the model: ")
print(model.output_shape)

# Save the model architecture to file
plot(model, to_file='./'+experiment_name+'/'+experiment_name + '_model.png')
# ^check how the model looks like
json_string = model.to_json()
open('./'+experiment_name+'/'+experiment_name + '_architecture.json',
     'w').write(json_string)

# Scheduler

"""
def step_decay(epoch):
    learning_rate = 0.01
    if epoch == 100:
        return 0.005
    else:
        return learning_rate


rate_drop = LearningRateScheduler(step_decay)
"""
# Begin training
checkpointer = ModelCheckpoint(filepath='./' + experiment_name + '/'
                               + experiment_name + '_best_wt.h5',
                               verbose=1, monitor='val_loss', mode='auto',
                               save_best_only=True)
model.fit(X_train, Y_train, epochs=N_epochs, batch_size=batch_size, verbose=2,
          shuffle=True, validation_split=0.1, callbacks=[checkpointer])

# Save the model and weights for testing
model.save_weights('./'+experiment_name+'/'+'final_wt.h5', overwrite=True)
